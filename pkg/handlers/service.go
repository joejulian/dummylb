/*
Copyright 2019 Joe Julian <me@joejulian.name>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package handlers

import (
	"context"

	"github.com/Sirupsen/logrus"

	api_v1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	"gitlab.com/joejulian/dummylb/pkg/utils"
)

// SetServiceLoadBalancer sets the Service Status LoadBalancer Ingress to a single IP of 127.0.0.1
func SetServiceLoadBalancer(service *api_v1.Service) error {
	var kubeClient kubernetes.Interface
	_, err := rest.InClusterConfig()
	if err != nil {
		kubeClient = utils.GetClientOutOfCluster()
	} else {
		kubeClient = utils.GetClient()
	}

	service.Status.LoadBalancer.Ingress = []api_v1.LoadBalancerIngress{{IP: "127.0.0.1"}}
	logrus.WithFields(logrus.Fields{
		"service":   service.Name,
		"namespace": service.Namespace,
		"status":    service.Status,
	}).Infof("setting status")
	service, err = kubeClient.CoreV1().Services(service.Namespace).UpdateStatus(context.Background(), service, v1.UpdateOptions{})
	if err != nil {
		logrus.WithError(err).Errorf("UpdateStatus Failed")
		return err
	}
	logrus.WithFields(logrus.Fields{
		"service":   service.Name,
		"namespace": service.Namespace,
		"status":    service.Status,
	}).Infof("configured")

	return nil
}
