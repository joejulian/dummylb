FROM golang:1.16 as builder
COPY ./ /go/src/gitlab.com/joejulian/dummylb
WORKDIR /go/src/gitlab.com/joejulian/dummylb
RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o dummylb-controller .

FROM scratch
COPY --from=builder /go/src/gitlab.com/joejulian/dummylb/dummylb-controller .
CMD ["/dummylb-controller"]